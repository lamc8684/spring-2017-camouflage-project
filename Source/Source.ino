int motorPin=A6;
float dutycycle=100;
int period=10;
String incoming;
int blinkPin = A2; //pin input to LED
int LED=13;
int on=0;

void setup()
{
  Serial.begin(9600);
  pinMode(motorPin, OUTPUT);
  pinMode(blinkPin, OUTPUT);
  pinMode(LED, OUTPUT);
  Serial.print('Enter period, duty cycle, or on for');
}

void loop()
{
 if (Serial.available()>0) {
  incoming=Serial.read(); 
  
  if(incoming.startsWith('p')){
    period=Serial.parseInt();
    setPumpPeriod(period);
  }
  else if(incoming.startsWith('d')){
  dutycycle=Serial.parseInt();
  setPumpDutyCycle((float) dutycycle);
  }

  else if(incoming.startsWith('o')){
  on=Serial.parseInt();
  pumpOnFor(on);
  }
}
}

//set pump duty cycle
 void setPumpDutyCycle(float duty_cycle)
 {   
   dutycycle=duty_cycle; //set the global duty cycle
   //analogWrite(motorPin, 2.56*duty_cycle); //256 is HIGH, duty cycle is % of HIGH, /100 to compensate 
   Serial.print(duty_cycle);
   Serial.println("% Duty Cycle");     
 }
 
 //set pump period
 void setPumpPeriod(int period_ms)
 {
   period=period_ms;
   //analogWrite(motorPin, 2.56*dutycycle);
   //delay(period_ms/2);
   //analogWrite(motorPin, 2.56*dutycycle);
   //delay(period_ms/2);
   Serial.print(period_ms);
   Serial.println(" ms Period");  
 }
 
 //Leave the pump on at the set duty cycle and period for a certain amount of time
 void pumpOnFor(int on_time_ms)
 {
  int starttime=millis();
  int endtime = millis();
  while((endtime-starttime)<=on_time_ms){
    analogWrite(motorPin, 2.55*dutycycle); 
    endtime=millis();
  }
 }
   
