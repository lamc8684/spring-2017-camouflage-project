/* This function controls both the pump and LED through the Teensy board
Blinks the LED pin on and off for a given period which can be increased by the 1 or 2 keys

Turns on the pump for a given, input Duty Cycle

Last Update: January 24, 2017
Lauren McIntire


*/

/* void setPumpDutyCycle(float duty_cycle);

void setPumpPeriod(int period_ms);

void pumpOnFor(int on_time_ms);
*/

int motorPin = A6; //pin input to motor
int blinkPin = A2; //pin input to LED
int LED=13;
int period=0;
long longinput=0;
int input=0;

//Enter 1 to decrease the period by 0.5 seconds, 2 to increase the period by 0.5 seconds and any value between 0 to 100 (excluding 1 and 2) for the percent duty cylce


void setup()
{
	pinMode(motorPin, OUTPUT);
	pinMode(blinkPin, OUTPUT);
        pinMode(LED, OUTPUT);
        Serial.begin(9600);
}
        
void loop()
{ 
        digitalWrite(LED, HIGH);
        if (Serial.available()>0) {
          longinput=Serial.parseInt();
          if (longinput==1){
            period-=500;
            Serial.print(period);
            Serial.println(" second Period");            
          }
          else if (longinput==2){
            period+=500;
            Serial.print(period);
            Serial.println(" second Period");     
          }
          else{
            input=(int)longinput;
            Serial.print(input);
            Serial.println("% Duty Cycle");     
          }
        }
        
        digitalWrite(blinkPin, HIGH);
        delay(period/2);
        digitalWrite(blinkPin, LOW);
	//analogWrite(motorPin, 2.56*input);
        delay(period/2);
//	digitalWrite(blinkPin, LOW);
	
}
